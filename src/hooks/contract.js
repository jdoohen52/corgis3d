import React, { useCallback, useState } from "react";

const BOATLOAD_OF_GAS = 300000000000000;
const PRICE = "3000000000000000000000000";
const UNIT = "000000000000000000000000";

export const ContractContext = React.createContext();

export const ContractContextProvider = ({ Contract, children }) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const [creating, setCreating] = useState("");
  const [transfering, setTransfering] = useState("");
  const [deleting, setDeleting] = useState("");
  const [selling, setSelling] = useState("");
  const [buying, setBuying] = useState("");

  const [corgis, setCorgis] = useState([]);
  const [corgi, setCorgi] = useState(null);
  const [displayCorgis, setDisplay] = useState([]);

  const createCorgi = useCallback(
    async (name, color, backgroundColor, quote) => {
      setCreating("start");
      try {
        await Contract.create_corgi(
          { name, color, background_color: backgroundColor, quote },
          BOATLOAD_OF_GAS,
          PRICE
        );
        setCreating("finish");
      } catch (error) {
        setCreating("over");
        console.error(error);
        setError(error);
      }
    },
    [Contract]
  );

  const transferCorgi = useCallback(
    async (receiver, id, message) => {
      setTransfering("start");
      try {
        await Contract.transfer_with_message(
          { new_owner_id: receiver, token_id: id, message },
          BOATLOAD_OF_GAS
        );
        setTransfering("finish");
      } catch (error) {
        setTransfering("over");
        console.error(error);
        setError(error);
      }
    },
    [Contract]
  );

  const deleteCorgi = useCallback(
    async (id) => {
      setDeleting("start");
      try {
        await Contract.delete_corgi({ id }, BOATLOAD_OF_GAS);
        setDeleting("finish");
      } catch (error) {
        setDeleting("over");
        console.log(error);
        setError(error);
      }
    },
    [Contract]
  );

  const sellCorgi = useCallback(
    (id, price) => {
      setSelling("start");
      Contract.sell_corgi({ id, price }, BOATLOAD_OF_GAS)
        .then(() => {
          setSelling("finish");
        })
        .catch((error) => {
          setSelling("over");
          console.log(error);
          setError(error);
        });
    },
    [Contract]
  );

  const buyCorgi = useCallback(
    (id, price) => {
      setBuying("start");
      Contract.buy_corgi({ id }, BOATLOAD_OF_GAS, price + UNIT)
        .then(() => {
          setBuying("finish");
        })
        .catch((error) => {
          setBuying("over");
          console.log(error);
          setError(error);
        });
    },
    [Contract]
  );

  const getCorgisList = useCallback(
    async (owner) => {
      setLoading(true);
      try {
        let corgis = await Contract.get_corgis_by_owner({ owner });
        setCorgis(corgis);
        setLoading(false);
      } catch (e) {
        setLoading(false);
        console.log(e);
      }
    },
    [Contract]
  );

  const getCorgiOwner = useCallback(
    async (id) => {
      return await Contract.get_token_owner({ token_id: id });
    },
    [Contract]
  );

  const getCorgi = useCallback(
    async (id) => {
      setLoading(true);
      try {
        let _corgi = await Contract.get_corgi({ id });
        let owner = await getCorgiOwner(_corgi.id);
        _corgi.owner = owner;
        setCorgi(_corgi);
        setLoading(false);
      } catch (e) {
        setLoading(false);
        console.log(e);
      }
    },
    [Contract, getCorgiOwner]
  );

  const getDisplayCorgis = useCallback(async () => {
    setLoading(true);
    try {
      let corgis = await Contract.display_global_corgis();
      setDisplay(corgis);
      setLoading(false);
    } catch (e) {
      setLoading(false);
      console.log(e);
    }
  }, [Contract]);

  const value = {
    loading,
    error,
    corgi,
    corgis,
    displayCorgis,
    creating,
    transfering,
    deleting,
    buying,
    selling,
    createCorgi,
    deleteCorgi,
    transferCorgi,
    sellCorgi,
    buyCorgi,
    getCorgi,
    getCorgisList,
    getDisplayCorgis,
    setTransfering,
  };

  return (
    <ContractContext.Provider value={value}>
      {children}
    </ContractContext.Provider>
  );
};

export default ContractContextProvider;
