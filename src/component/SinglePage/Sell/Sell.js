import React, { useState, useContext } from "react";

import { SmallCard } from "../../CorgiCard/Card";
import Modal from "../../utils/Modal";

import { ContractContext } from "../../../hooks/contract";
import Button from "../../utils/Button";
import Spinner from "../../utils/Spinner";

const Sell = ({ corgi, show, closeModal }) => {
  const [price, _setPrice] = useState("");
  const useContract = useContext(ContractContext);
  const { sellCorgi, selling } = useContract;
  let l = window.location.pathname.split("/");
  const id = Number(l[l.length - 1]);

  const setPrice = (event) => {
    if (event) {
      const value = event.target !== null ? event.target.value : "";
      _setPrice(value);
    }
  };

  const onSubmit = (e) => {
    e.preventDefault();
    sellCorgi(id, price);
  };

  return (
    <Modal show={show} Close={closeModal}>
      {selling === "start" ? (
        <div style={{ width: "100%", height: "100%", marginBottom: "30px" }}>
          Selling
          <Spinner />
        </div>
      ) : (
        <div style={{ width: "100%", height: "100%", marginBottom: "30px" }}>
          <h3 style={{ marginBottom: "0" }}>Sell Corgi</h3>
          {corgi.selling && (
            <p style={{ color: "orange", fontSize: "0.7rem" }}>
              On sale for {corgi.selling_price} Ⓝ
            </p>
          )}
          <div>
            <div style={{ width: "100%", height: "90%" }}>
              <SmallCard
                backgroundColor={corgi.background_color}
                color={corgi.color}
                sausage={corgi.sausage}
                quote={corgi.quote}
              />
            </div>
            <p style={{ margin: "0" }}>{corgi.name}</p>
            <span style={{ color: "orange", fontSize: "0.7rem" }}>
              {corgi.rate}
            </span>
            <hr />
          </div>
          {selling === "finish" ? (
            <div style={{ marginBottom: "20px" }}>
              This corgi is now on market!
            </div>
          ) : (
            <div style={{ marginBottom: "20px" }}>
              <form onSubmit={onSubmit}>
                <div style={{ textAlign: "left", marginBottom: "3px" }}>
                  <label>Price: </label>
                  <input
                    autoFocus
                    required
                    type="text"
                    placeholder=""
                    value={price}
                    onChange={setPrice}
                    className="receiver"
                  />
                </div>
                <div style={{ marginTop: "5px", marginBottom: "10px" }}>
                  <Button description="Sell" />
                </div>
              </form>
            </div>
          )}
        </div>
      )}
    </Modal>
  );
};

export default Sell;
