import React, { useEffect, useContext, useState } from "react";

import useCharacter from "../../../hooks/character";
import { ContractContext } from "../../../hooks/contract";

import Button from "../../utils/Button";

import { FaExchangeAlt } from "react-icons/fa";

const generate = require("project-name-generator");
const randomColor = require("randomcolor");

const Info = ({ setColor, color, setBackgroundColor, backgroundColor }) => {
  const { name, quote, setName, setQuote } = useCharacter();
  useEffect(() => setQuote(), [setQuote]);
  const useContract = useContext(ContractContext);
  const { createCorgi } = useContract;
  const [inputQuote, setInputQuote] = useState("");

  const generateName = (e) => {
    setName(e.target.value);
  };

  const generateRandomName = () => {
    let name = generate({ words: 2, alliterative: true }).spaced;
    setName(name);
  };

  const generateColor = (e) => {
    setColor(e.target.value);
  };

  const generateBackgroundColor = (e) => {
    setBackgroundColor(e.target.value);
  };

  const generateRandomColor = () => {
    const color = randomColor();
    const backgroundColor = randomColor();
    setColor(color);
    setBackgroundColor(backgroundColor);
  };

  const generateQuote = (e) => {
    setInputQuote(e.target.value);
  };

  const onSubmit = (e) => {
    e.preventDefault();
    let _quote = inputQuote === "" ? quote : inputQuote;
    createCorgi(name, color, backgroundColor, _quote);
  };

  return (
    <div className="inputboard">
      <form onSubmit={onSubmit}>
        <div style={{ display: "flex", flexDirection: "column" }}>
          <div style={{ display: "flex" }}>
            <p className="title">My Corgi is called</p>
            <FaExchangeAlt onClick={generateRandomName} className="icon" />
          </div>
          <div>
            <input
              className="inputname"
              type="text"
              value={name}
              onChange={generateName}
              required
            />
          </div>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <div style={{ display: "flex" }}>
            <p className="title">Colors</p>
            <FaExchangeAlt onClick={generateRandomColor} className="icon" />
          </div>
          <div style={{ display: "flex" }}>
            <div className="colorpicker">
              <label>
                <div className="result" style={{ backgroundColor: color }}>
                  <input
                    type="color"
                    id="colorPicker"
                    value={color}
                    onChange={generateColor}
                    style={{
                      display: "none",
                    }}
                  />
                </div>
              </label>
              <div>
                <p className="icon-title">Corgi</p>
                <p className="icon-text">{color}</p>
              </div>
            </div>
            <div className="colorpicker">
              <label>
                <div
                  className="result"
                  style={{ backgroundColor: backgroundColor }}
                >
                  <input
                    type="color"
                    id="backgroundcolorPicker"
                    value={backgroundColor}
                    onChange={generateBackgroundColor}
                    style={{
                      display: "none",
                    }}
                  />
                </div>
              </label>
              <div>
                <p className="icon-title">Background</p>
                <p className="icon-text">{backgroundColor}</p>
              </div>
            </div>
          </div>
        </div>
        <div style={{ marginBottom: "20px" }}>
          <p className="title">Quote</p>
          <div>
            <input
              className="inputname"
              type="text"
              value={inputQuote}
              onChange={generateQuote}
            />
          </div>
        </div>
        <Button description="Mint Corgi with 3 Ⓝ" />
      </form>
      <p className="quote">
        This will create a one-of-a-kind Corgi that will develop a unique size
        and thought process. The size it grows to will untimately determine it’s
        value
      </p>
      <style>{`
            .inputboard {
                background: #f8f8f8;
                border-radius: 10px;
                padding: 1%;
                text-align: left;
                width: 50%;
                margin: 1%;
              }
              
              .title {
                font-weight: 600;
                font-size: 1.5em;
                margin: 10px 0;
                display: block;
              }
              
              .inputname {
                background: #ffffff;
                box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
                border-radius: 5px;
                width: 80%;
              }
              
              .quote {
                  font-size: 0.7em;
                  font-weight: thin;
                  display: inline-block;
                  margin-top: 20px;
              }
              
              @media all and (max-width: 765px) {
                .inputboard{
                  width: 100%;
                }
              }
              .colorpicker {
                display: flex;
                flex-flow: row;
              }
              
              .result{
                border: 2px solid #24272a;
                box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
                border-radius: 5px;
                width: 50px;
                height: 50px;
                cursor: grab;
              }
              
              #color-picker {
                display: none;
              }
              
              .select {
                cursor: pointer;
                position: relative;
                left: 29px;
                top: 24px;
                z-index: 10;
              }

              .icon-title {
                  margin: 0 2px;
                  font-weight: 600;
              }

              .icon-text {
                margin-top: 0;
                margin-left: 2px;
              }

              .icon {
                margin-top: 10px;
                margin-left: 10px;
                color: #a51cea;
                font-size: 1.5rem;
                border-radius: 50%;
                box-shadow:0 0 3px 3px rgba(0, 0, 0, 0.2), inset 0 0 5px 2px #ffffff;
                background: #f5ebff;
                cursor: pointer;
              }
            `}</style>
    </div>
  );
};
export default Info;
