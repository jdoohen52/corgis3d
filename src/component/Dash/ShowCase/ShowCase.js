import { Link } from "react-router-dom";
import React from "react";

import DashCard from "./DashCard/DashCard";

const ShowCase = ({ displayCorgis, text }) => {
  let Corgis = displayCorgis.map((corgi) => {
    return (
      <Link to={`/share/${corgi.id}`} key={corgi.id}>
        <DashCard corgi={corgi} />
      </Link>
    );
  });
  return (
    <div>
      <h2>{text}</h2>
      {Corgis}
    </div>
  );
};

export default ShowCase;
